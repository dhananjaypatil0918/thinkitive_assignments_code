from selenium.webdriver.common.by import By
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import *
from selenium.webdriver.support import expected_conditions as EC


class Search_page():

    def __init__(self, driver):
        self.driver = driver

# Locators
    _searchBoxField = "search"
    _searchButton = "searchButton"
    _search_result_text = "//span[contains(text(),'Search Results for')]"
    _products = "//a[contains(@class, 'clip-prd-dtl')]"
    _product_prices = "//span[@class='clip-offr-price ']"
    _products_all_data = "//div[@class='clip-crd-10x11 pf-white srch-rslt-bxwrpr']//div//a"
    popUp1 = "//div[@id='webklipper-publisher-widget-container-notification-close-div']"
    popUp_2 = "//body/div[@id='reg_login_box']/div[@id='regPopUp']/a[1]"

    def handle_popUp(self):
        """
        This method handles pop ups on webpage.
        :return: None
        """
        try:
            self.driver.switch_to.frame(
                "webklipper-publisher-widget-container-notification-frame")
        except BaseException:
            print('Iframe Not found')

        try:
            self.wait_for_element(self.popUp1).click()
        except BaseException:
            "PopUp 1 not found"
        self.driver.switch_to.parent_frame()
        self.wait_for_element(self.popUp_2).click()

    def searchProduct(self, product_name):
        """
        :param product_name: Provide product name for searching.
        :return: None
        """
        search = self.driver.find_element_by_id(self._searchBoxField)
        search.send_keys(product_name)
        self.driver.find_element_by_id(self._searchButton).click()

    def verify_search_result(self):
        """
        This method for assetion of search result found
        :return: True/False
        """
        res = self.driver.find_element_by_xpath(self._search_result_text)
        result = res.is_displayed()
        return result

    def wait_for_element(self, element):
        """
        :param element: Provide element to put explicit wait.
        :return: element
        """
        wait = WebDriverWait(
            self.driver,
            10,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotInteractableException,
                NoSuchElementException,
                ElementNotVisibleException])
        found_element = wait.until(EC.element_to_be_clickable((
            By.XPATH, element)))
        return found_element

    def parse_product_data(self):
        """
        This method parse the product names and their prices from the web page
        Compare the prices and print the highest price product
        :return: max product with price.
        """
        self.wait_for_element(self._products)
        self.driver.execute_script(
            "window.scrollTo(0,document.body.scrollHeight)")
        time.sleep(3)
        products_List = self.driver.find_elements_by_xpath(self._products)
        products_prices_List = self.driver.find_elements_by_xpath(
            self._product_prices)

        product_name = [product.text for product in products_List]
        product_prices = [price.text for price in products_prices_List]
        clean_prices = []
        for price in product_prices:
            remove_symbol = price.replace('₹ ', '')
            remove_comma = remove_symbol.replace(',', '')
            clean_prices.append(int(remove_comma))
        result = {}
        for key in product_name:
            for value in clean_prices:
                result[key] = value
                clean_prices.remove(value)
                break
        Keymax = max(result, key=result.get)
        print(
            f'Product with the Highest price = \n{Keymax} : {result[Keymax]}')

    def write_all_product_information_to_file(self):
        """
        This method find list of elements and find properties of those elements and write into
        text file
        :return: None
        """
        allProducts = self.driver.find_elements_by_xpath(
            self._products_all_data)
        ProductsData_List = [product.get_attribute(
            'data-params') for product in allProducts]
        ProductsHref_List = [product.get_attribute(
            'data-params') for product in allProducts]
        allProductsData_List = zip(ProductsData_List, ProductsHref_List)
        with open('all_product_data_result.txt', 'w', encoding='utf-8') as f:
            for item in allProductsData_List:
                f.write(str(item) + '/n')
        print('All Product details are stored in the "all_product_data_result.txt" file.')

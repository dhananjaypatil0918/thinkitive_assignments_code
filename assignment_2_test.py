# Assignment 2
from selenium import webdriver
from pepperfry_search import Search_page


class Test_Script():
    baseURL = "https://www.pepperfry.com/"
    driver = webdriver.Chrome(r"C:\bin\chromedriver.exe")
    driver.maximize_window()
    driver.implicitly_wait(10)
    driver.get(baseURL)
    sp = Search_page(driver)

    def test_automatedScript(self):
        self.sp.handle_popUp()
        self.sp.searchProduct("office chair")
        search_found = self.sp.verify_search_result()
        assert search_found, 'Search Result Not Found'
        self.sp.parse_product_data()
        self.sp.write_all_product_information_to_file()

# Assignment 1

def multiplier(num):
    """
    This method read text file and multiply the number by num
    :param num: Provide multiplier number
    :return: writes multiplication result in 'result.txt'.
    """
    l1 = []
    with open("input (1).txt", 'r') as fr:
        with open("result.txt", 'w') as fw:
            for line in fr:
                currentLine = line.split('|')
                for j in currentLine:
                    x = j.replace('\n', '')
                    l1.append(x)
            result = [int(i) * num for i in l1]
            convert_to_str = [str(number) + '\n' for number in result]
            fw.writelines(convert_to_str)


multiplier(7)
